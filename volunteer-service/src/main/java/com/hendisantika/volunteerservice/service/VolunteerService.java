package com.hendisantika.volunteerservice.service;

import com.hendisantika.volunteerservice.bootstrap.DataInitializer;
import com.hendisantika.volunteerservice.domain.Volunteer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : consul-microservice-discovery-sample2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-20
 * Time: 05:54
 */
public class VolunteerService {

    private static Logger logger = LogManager.getLogger(VolunteerService.class);

    @Autowired
    DataInitializer initializer;

    public VolunteerService() {
        super();
        // this.initializer = initializer;
    }

    public List<String> findVolunteers(String servicename) {
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%");
        List<String> volnames = new ArrayList<String>();
        List<Volunteer> vols = DataInitializer.vols;
        for (Volunteer vol : vols) {
            logger.info(vol.getServices() + ":" + vol.getFirstname());
            if (vol.getServices().contains(servicename)) {
                volnames.add(vol.getFirstname() + " " + vol.getLastname());
            }

        }
        logger.info("FFFFFFFFFFFFFFFFFF");

        return volnames;
    }
}
