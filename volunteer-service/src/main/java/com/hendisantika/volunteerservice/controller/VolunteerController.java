package com.hendisantika.volunteerservice.controller;

import com.hendisantika.volunteerservice.service.VolunteerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : consul-microservice-discovery-sample2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-20
 * Time: 06:05
 */
@RestController
public class VolunteerController {
    private static Logger logger = LogManager.getLogger(VolunteerService.class);

    @GetMapping("/")
    public String home() {
        return "Hello World from Volunteer application";
    }

    @RequestMapping(value = "/volunteers/{servicename}", method = RequestMethod.GET)
    public List<String> listVolunteers(@PathVariable("servicename") String servicename) {
        logger.info("listVolunteers");
        VolunteerService service = new VolunteerService();
        return service.findVolunteers(servicename);

    }
}