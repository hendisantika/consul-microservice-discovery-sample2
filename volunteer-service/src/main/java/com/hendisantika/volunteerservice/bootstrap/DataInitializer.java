package com.hendisantika.volunteerservice.bootstrap;

import com.hendisantika.volunteerservice.domain.Volunteer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : consul-microservice-discovery-sample2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-20
 * Time: 05:54
 */
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
@Component
public class DataInitializer {

    public static List<Volunteer> vols = new ArrayList<Volunteer>();
    private static Logger logger = LogManager.getLogger(DataInitializer.class);


    public DataInitializer() {
        super();
        logger.info("Initializing data...");
        initializeData();
        // TODO Auto-generated constructor stub
    }

    public void initializeData() {
        Volunteer vol1 = new Volunteer();
        Volunteer vol2 = new Volunteer();
        Volunteer vol3 = new Volunteer();

        vol1.setFirstname("Uzumaki");
        vol1.setLastname("Naruto");
        vol1.addService("droptoschool");
        vol1.addService("donateblood");

        vol2.setFirstname("Uchiha");
        vol2.setLastname("Sasuke");
        vol2.addService("droptoschool");
        vol3.setFirstname("Sakura");
        vol3.setLastname("Haruno");
        vol3.addService("donateblood");
        vols.add(vol1);
        vols.add(vol2);
        vols.add(vol3);

        logger.info("Number of Volunteers:" + vols.size());
    }

}